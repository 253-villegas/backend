const express = require("express");
// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose")

const app = express();
const port = 4000;



// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application
// { newUrlParser : true } allows us to avoid any current and future errors

// npm install mongoose@5.12.0

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin123@batch253-villegas.27ddqso.mongodb.net/s35?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true

})


let db = mongoose.connection;
// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log(`We're now connected to the cloud database: MongoDB Atlas!`));

// ============== Mongoose Schemas ===============
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// for a task it needs a task name status
	// we have the name field with a data type of string
	name: String,
	// field called "status" with a data type of "string and deafault value of "pending"
	status: {
		type: String,
		// Deafault values are the predefined values for a field if we don't put any value
		default: "pending"
	}
});


// ============== Models ==============
// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models use Schemas and they act as the middleman from the server (JS code) to our database
const Task = mongoose.model("Task", taskSchema);


// Setup for allowing the server to handle data from requests
// Allows your app to read json data
app.use(express.json());
// Allows your app to read data from forms
app.use(express.urlencoded({ extended: true }));


app.post("/tasks", (req, res) => {

	// Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria
	// If there are no matches, the value of result is null
	// "err" is a shorthand naming convention for errors
	Task.findOne({ name : req.body.name }) .then(result => {
		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.name == req.body.name){
			// Return a message to the client/Postman
			return res.send("Duplicate task found.")
			// If no document was found
		} else {
			// Create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});


			//.then and .catch chain:
			//.then() is used to handle the proper result/returned value of a function. If the function properly returns a value, we can run a separate function to handle it.
			//.catch() is used to handle/catch the error from the use of a function. So that if there is an error, we can properly handle it separate from the result.
			newTask.save()
				.then(result => res.send(result))
				.catch(error => res.send(error));

		}
	}).catch(error => res.send(error))
})

// Getting all the tasks

// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) =>{

	Task.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
});

/*


//============= Delete All Tasks =============== 
app.delete("/delete-tasks", (req, res) => {

	Task.deleteMany({})
		.then(result => {
			if (result.deletedCount > 0) {
				// If at least one Task was deleted, send a success message
				const message = `${result.deletedCount} Tasks have been deleted.`;
				res.send(message);
			} else {
				// If no Tasks were found, send an error message
				const message = "No users found.";
				res.status(404).send(message);
			}
		})
		.catch(err => {
			// If an error occurred, send an error message
			const message = "Error deleting users.";
			res.status(500).send(message);
		});
});


*/





// ================= ACTIVITY ==================
// USER SCHEMA
const userSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// for a task it needs a task name status
	// we have the name field with a data type of string
	username: String,
	// field called "status" with a data type of "string and deafault value of "pending"
	password: String,

	
});
// USER MODEL
const User = mongoose.model("User", userSchema);

// SIGNUP ROUTE

// create a POST route to register a new user
app.post("/signup", (req, res) => {
  // check if the user already exists
  User.findOne({ username: req.body.username})
    .then(result => {
      // if the user already exists, return an error message
      if (result !== null && result.username === req.body.username) {
        return res.send("Duplicate username found");
      } else {
        // check if both username and password are provided
        if (req.body.username && req.body.password) {
          // create a new user and save it to the database
          let newUser = new User({
            username: req.body.username,
            password: req.body.password,

          });

          newUser
            .save()
            .then(result => {
              // return a success message and status code 201 for created
              return res.status(201).send("New user registered");
            })
            .catch(error => {
              // log any errors in the console
              console.log(error);
              return res.send(error);
            });
        } else {
          // if either username or password is missing, return an error message
          return res.send("BOTH username and password must be provided.");
        }
      }
    })
    .catch(error => {
      // log any errors in the console
      console.log(error);
      return res.send(error);
    });
});





/*


//=========== Delete One User ===========

app.delete("/delete", (req, res) => {

	User.findOneAndDelete({ username : req.body.username, password : req.body.password })
		.then(result => {
			if (result) {
				// If a user was found and deleted, send a success message
				const message = `User ${req.body.username} has been deleted.`;
				res.send(message);
			} else {
				// If no user was found, send an error message
				const message = "No user found.";
				res.status(404).send(message);
			}
		})
		.catch(err => {
			// If an error occurred, send an error message
			const message = "Error deleting user.";
			res.status(500).send(message);
		});
});





//============= Delete ALl Users =============== 
app.delete("/delete-many", (req, res) => {

	User.deleteMany({})
		.then(result => {
			if (result.deletedCount > 0) {
				// If at least one user was deleted, send a success message
				const message = `${result.deletedCount} users have been deleted.`;
				res.send(message);
			} else {
				// If no users were found, send an error message
				const message = "No users found.";
				res.status(404).send(message);
			}
		})
		.catch(err => {
			// If an error occurred, send an error message
			const message = "Error deleting users.";
			res.status(500).send(message);
		});
});

app.delete("/delete-all-tasks", (req, res) => {

	User.findOneAndDelete({ name : req.body.name})
		.then(output => {
			if (output) {
				// If a user was found and deleted, send a success message
				const message = `User ${req.body.name} has been deleted.`;
				res.send(message);
			} else {
				// If no user was found, send an error message
				const message = "No user found.";
				res.status(404).send(message);
			}
		})
		.catch(err => {
			// If an error occurred, send an error message
			const message = "Error deleting user.";
			res.status(500).send(message);
		});
});


*/



app.get("/user", (req, res) =>{

	User.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
});

// Listen to the port, meaning, if the port is accessed, we run the server
app.listen(port, () => console.log(`Server running at location:${port}...`))