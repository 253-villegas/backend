const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");

// Allows access to routes defined within our application
const userRoute = require("./routes/userRoute")




// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();
const port = process.env.PORT || 4000
const db = mongoose.connection

// Connect to MOngoDB database
mongoose.connect("mongodb+srv://admin:admin123@batch253-villegas.27ddqso.mongodb.net/course-bookingAPI?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

db.once("open", () => console.log("Now connected to MongoDB Atlas."));


app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoute);


if(require.main === module){
	app.listen(port, () => {
		console.log(`API is now online on port ${ port }`)
	});
}

module.exports = app;