const Course = require("../models/Course");
const User = require("../models/User");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/
//Session 39 - Activity Solution 1
module.exports.addCourse = (reqBody) => {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		return newCourse.save().then(course => true)
			.catch(err => false);
};

//Session 39 - Activity Solution 2
/*module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return false
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return err
                } else {
                    //course creation successful
                    return true;
                }
            })
        }
        
    });    
}*/


// Retrieve all courses
/*
	Steps:
	1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result)
		.catch(err => err);
};

module.exports.getAllActive = () => {

	return Course.find({ isActive : true }).then(result => result)
		.catch(err => err);
};

// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided from the URL
*/
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	}).catch(err => err);

};

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		Syntax:
			ModelName.findByIdAndUpdate(documentId, updatesToBeApplied).then(statement)

			req.params = { courseId: 613e926a82198824c8c4ce0e}
			req.params.id = 613e926a82198824c8c4ce0e
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
		.then(course => true).catch(err => err);
};

// Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
//Session 40 - Activity Solution 1
module.exports.archiveCourse = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive	
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then(course => true).catch(err => err);
};

//Session 40 - Activity Solution 2
/*module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then(course => true).catch(err => err);
};*/

