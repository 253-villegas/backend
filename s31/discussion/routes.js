// ctrl + c = stop the server
// localhost:4000/greeting (open in browser)

const  http = require("http");

// 
const port = 4000;

const server = http.createServer((request, response) => {
	//Accessing the "greeting" route returns a message of "Hello World"

	if(request.url == "/greeting"){
		response.writeHead(200, { "content-Type": "text/plain"});
		response.end("Hello Again!");

	} else if (request.url == "/homepage"){
		// Accessing the "homepage" route returns a message of "This is the homepage"
		response.writeHead(200, { "Content-Type": "text/plain"});
		response.end("This is the homepage");
	} else {
		//  Set a status code for the response - a 404 means Not Found
		response.writeHead(400, { "Content-Type": "text/plain"});
		response.end("Page not available"); 
	}
});

server.listen(port);

console.log(`Server now accessible at localhost:${port}.`);
