
const  http = require("http");


const port = 3000;

const server = http.createServer((request, response) => {
	
	//Accessing the "login" route returns a message of "Hello World"
	if(request.url == "/login"){
		response.writeHead(200, { "content-Type": "text/plain"});
		response.end("Welcome to login page.");

	} else {
		//  Set a status code for the response - a 404 means Not Found
		response.writeHead(400, { "Content-Type": "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found."); 
	}
});

server.listen(port);

console.log(`Server now accessible at localhost:${port}.`);

