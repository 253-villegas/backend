// console.log("Hello!");

//Functions
	
		//Parameters and Arguments

			// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
			// Functions are mostly created to create complicated tasks to run several lines of code in succession
			// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

			//We also learned in the previous session that we can gather data from user input using a prompt() window.

function printInput(){
	let	nickname = prompt("Enter your nickname");
	console.log("Hi, " + nickname);
}

// printInput();

function printName(name){
	console.log("My name is " + name);

}

printName("Jay Villegas");
printName("Enma Ai");
printName("Kudo Shinichi");

// variables can also be passed as an argument
let sampleName = "Yui";
printName(sampleName);


// function checkDivisibleBy8(divsible){
// 	let num = 165
// 	let	remainder = num % 8;
	
// 	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
// 	console.log("Is " + num + " divisible by 8?");
// 	let isDivsibleBy8 = remainder === 0;
// 	console.log(isDivisibleBy8);

// };

// checkDivisibleBy8(64);
// checkDivisibleBy8(28);




// function as arguments
function argumentFunction(argumentFunction){
	console.log("This function was passed as an argument before the message is printed.");
}

function invokeFuncion(argumentFunction){
	argumentFunction();
}
// invokeFunction(argumentFunction);

// Multiple Parameters
function createFullname(firstName, middleName, LastName){
	console.log(firstName + " " + middleName + " " + LastName);
};

createFullname("Juan", "Dela", "Cruz");
createFullname("Juan", "Dela");//if argument is lesser than the parameters, the last argument that is not included will return as undefined
createFullname("Juan", "Dela", "Cruz", "Hello");//if the argument is more than the parameters, the exceeding will not dispaly

let	firstName = "John";
let	middleName = "Doe";
let	LastName = "Smith";

createFullname(firstName, middleName, LastName);

//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.

// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, LastName){
	return firstName + " " + middleName + " " + LastName;

	console.log("Cute ko");
	// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.
};
let completeName = returnFullName("Lucy", " ", "Pevensie");
console.log(completeName);

console.log(returnFullName (firstName, middleName, LastName));



function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let address = returnAddress("Shire", "Middle Earth");
console.log(address);

function printPlayerInfo(username, level, jobclass){

	// console.log("Username: " + username);
	// console.log("Level1: " + level);
	// console.log("Job: " + jobclass);

	return "Username: " + username + "\n" + "Level: " + level + "\n" + "Job: " + jobclass;

}
let user1 = printPlayerInfo("janicchi", 95, "Rogue");
console.log(user1);

//returns undefined because printPlayerInfo returns nothing. It only console.logs the details. 

			//You cannot save any value from printPlayerInfo() because it does not return anything.





















