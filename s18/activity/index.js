// console.log("Hello! World");

function addNum(num1, num2){

    console.log("Displayed sum of " + num1 + " and " + num2);
    console.log(num1 + num2);
}
addNum(5, 15);

function subNum(num1, num2){
    console.log("Displayed difference of " + num1 + " and " + num2);
    console.log(num1 - num2);
}

subNum(20, 5);


function multiplyNum(num1, num2){
    console.log("The product of " + num1 + " and " + num2 + ":");
    return num1 * num2;
}

let product = multiplyNum(50, 10);
console.log(product);


function divideNum(num1, num2){
    console.log("The quotient of " + num1 + " and " + num2 + ":");
    return num1 / num2;
}
let quotient = divideNum(50,10);
console.log(quotient);


function getCircleArea(radius) {
    console.log("The result of getting the area of a circle with " + radius + " radius:");
    let result = (Math.PI * radius * radius);
    return result.toFixed(2);
}
let circleArea = getCircleArea(15);
console.log(parseFloat(circleArea));


function getAverage(num1,num2,num3,num4){
    console.log("The average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":");
    return (num1 + num2 + num3 + num4) / 4;
}
let averageVar = getAverage(20,40,60,80);
console.log(averageVar);


function checkIfPassed(score, total){
    console.log("Is " + score + "/" + total + " a passing score?");
    let passingPercentage = (score / total) * 100;
    return passingPercentage >= 75;
}
let isPassingScore = checkIfPassed(38,50);
console.log(isPassingScore);








//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}