const Product = require("../models/Product");
const User = require("../models/User");

// ========== Create product ==========
/*module.exports.addProduct = (reqBody) => {

		let newProduct = new Product({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		return newProduct.save().then(product => `Successfully Added the Product ${reqBody.name}`)
			.catch(err => false);
};*/

module.exports.addProduct = (reqBody) => {

	return Product.find({name : reqBody.name}).then(result => {

		if(result.length > 0) {

			return "Duplicate Product"

		} else{

			let newProduct = new Product({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price,
		});

			return newProduct.save().then(user => {

			    if(user){

			        return `Successfully Added the Product ${reqBody.name}`;
			        
			    } else {

			        return "Try Again!";
			    }
			}).catch(err => err)
		}
	}).catch(err => err);
};

// ========== Getting all products ==========
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result)
		.catch(err => err);
};

// ========== Getting all active pruducts ==========
module.exports.getAllActiveProducts = () => {

	return Product.find({ isActive : true }).then(result => result)
		.catch(err => err);
};

// ========== Getting single product ==========
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	}).catch(err => err);

};

// ========== Update products ==========
module.exports.updateProduct = (reqParams, reqBody) => {

		let updateProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updateProduct)
		.then(course => true).catch(err => err);
};

// ========== Archive product ==========
module.exports.archiveProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive	
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(product => true).catch(err => err);
};

// ========== Activate products ==========
module.exports.ativateProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive	
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(product => true).catch(err => err);


};
