const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require('../models/Order');

// ========== User registration ==========
module.exports.registerUser = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		if(result.length > 0) {

			return "Email already exist"

		} else{

			let newUser = new User({
			    firstName : reqBody.firstName,
			    lastName : reqBody.lastName,
			    email : reqBody.email,
			    password : bcrypt.hashSync(reqBody.password, 10),

			});

			return newUser.save().then(user => {

			    if(user){

			        return ` Welcome ${reqBody.firstName} ${reqBody.lastName} `;
			        
			    } else {

			        return "Try Again!";
			    }
			}).catch(err => err)
		}
	}).catch(err => err);
};

// ========== User login ==========
module.exports.loginUser = (reqBody) => {

	return User.findOne({ email : reqBody.email }).then(result => {

		if(result == null){

			return "Not Yet Registered";

		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){

				return { access : auth.createAccessToken(result) }

			} else {

				return false;
			}
		}
	}).catch(err => err);
}

// ========== Get all users ==========
module.exports.getAllUsers = () => {
	return User.find({}).then(result => result)
		.catch(err => err);
};

/*
module.exports.orderProduct = async (data) => {

	let isUserUpdated = await User.findById(data.userId)
		.then(user => {

			user.orderedProduct.push({ 

				productId : data.productId,
				// productName : data.productName,
				// quantity : data.quantity

			});

			return user.save().then(user => true)
				.catch(err => err)
		});

	let isProductUpdated = await Product.findById(data.productId)
		.then(product => {

			product.userOrders.push({ userId : data.userId });

			return product.save().then(product => true)
				.catch(err => false);

		});

	if(isUserUpdated && isProductUpdated){
		return true;

	} else {
		return "failed"
	}

}*/

// Order Checkout
module.exports.checkout = async (reqBody, data) => {

    let productName = await Product.findOne({ name : reqBody.name }).then(result => result.id).catch(err => err)

    let productPrice = await Product.findOne({ name : reqBody.name }).then(result => result.price).catch(err => err)

    if(productName !== null){

        let newOrder = new Order({

            userId : data,
            products : [{ productId : productName, quantity : reqBody.quantity }],
			totalAmount : productPrice * reqBody.quantity

        })
        return newOrder.save().then(order => true).catch(err => err)
    } else {
        return "Try again!"
    }
};

// Get user details
module.exports.getUser = (reqParams) => {

	return User.findById(reqParams.userId).then(result => {

		result.password = "";

		return result;

	});

};
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		result.password = "";
		return result;

	});

};



// Retrieve user orders
module.exports.userOrders = (reqParams) => {

	return Order.findById(reqParams.orderId).then(order => order).catch(err => err)
	console.log(order)
};


