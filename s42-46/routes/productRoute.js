const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");


// Route for creating a product
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});


//  Route for getting all products
router.get("/all", (req, res) => {

	productController.getAllProducts(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

});

// Route for getting all active products
router.get("/", (req, res) => {

	productController.getAllActiveProducts(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

});

// Route for retrieving single product
router.get("/:productId", (req, res) => {

	console.log(req.params);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));


});

// Route for updating product information
router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(req.params);

	if(userData.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});


// Route for archiving products
router.put("/:productId/archive", auth.verify, (req, res) => { //need middleware

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});

router.put("/:productId/activate", auth.verify, (req, res) => { //need middleware

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.ativateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;