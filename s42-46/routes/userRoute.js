const express = require("express");
const router = express.Router();
const  userController = require("../controllers/userController");
const auth = require("../auth");

// Route for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
			
});

// Route for login User
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

//  Route for getting all users details
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		userController.getAllUsers(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});

// Route for checkout
/*router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId : userData.id,
		isAdmin : userData.isAdmin,
		productId : req.body.productId,
		productName : userData.name,
		quantity : 1
	}

		if(!data.isAdmin){

			userController.orderProduct(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
		} else {
		res.send("Admin not allowed to enroll")
		}
	
})*/

// Route for checkout
router.post('/checkout', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(!userData.isAdmin){

    	userController.checkout(req.body, userData.id).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
        
    } else {

        res.send("Change to user account")
    }
});


// retrieve user details
router.get("/:userId/userDetails", (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getUser(userData.id).then(
		resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});



router.post('/orders', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(!userData.isAdmin){

    	userController.userOrders(req.body, userData.id).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
        
    } else {

        res.send("Change to user account")
    }
});




// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;