db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
		]);


//  MongoDB Aggreation Method
// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data

/*
	- The "$match" is used to pass the documents that meet the specified conditions
		Syntax: { $match: { "field": "value"}}
	- The "$group" is use to group the element together
		Syntax: { $group: { "_id": "value", "fieldResult": "valueResult"}}
*/

//  Single_Purpose Aggregation Operations

db.fruits.count();

// Pipelines with multiple stages
db.fruits.aggregate([
	{ $match: {
		"onSale": true
	}},
	{ $group:
		{ "_id": "$supplier_id",
			"total": { $sum: "$stock"}
	}}
])
//  Field Projection with Aggregation
//  $project can be used aggregating data to include/exclude field from the returned results
/*
	Syntax:
		{ $project: {"field": 1/0 }}

*/

db.fruits.aggregate([
	{ $match: { "onSale": true}},
	{ $group: { "_id": "$supplier_id", "total": { $sum: "$stock"}}},
	{ $project: { "_id": 0}}

]);
// MINI-ACTIVITY
/*
	aggeregate all fruits that has a stock greater than or equal 20
	group by supplier id 
	show the sum of all stocks
*/


db.fruits.aggregate([
	{ $match: { "stock": {$gte: 20}}},
	{ $group: { "_id": "$supplier_id", "total": { $sum: "$stock"}}},

]);


/*
	- The "$sort" can be used to change the order of aggregated results
	- Providing a value of -1 will sort the aggregated results in a reverse order


*/

db.fruits.aggregate([
	{ $match: { "onSale": true}},
	{ $group: { "_id": "supplier_id", 
				"total": { $sum: "$stock"}}},
	{ $sort: { "total": -1}}

])
// Computations for aggregated results
// used in $group staging
/*
	$sum - to total the values
	$avg - to total the average of values
	$min - minimum value of the field
	$max - maximum value of the field

	Syntax:
		$group: { 
			"_id": "fieldToBeGrouped",
			"fieldForResults": { $sum/max/min/avg: "$stock"} 
		}

*/
// Aggregating results on array fields
/*- 
	The "$unwind" deconstructs an array field from a collection/field with an array value to output a result for each element.
	Syntax:
		{ $unwind: "$field"}
*/
db.fruits.aggregate([
	{ $unwind: "$origin"}
]);

db.fruits.aggregate([
	{
		$unwind: "$origin"
	},
	{
		$group: {
			"_id": "$origin",
			"kinds": { $sum: 1 }
		}
	}
]);

//  ======= Schema Design ======
let owner = ObjectId();

db.owners.insertOne({
	"_id": owner,
	"name": "John Smith",
	"contact": "09123456789"
});

db.suppleirs.insertOne({
	"name": "ABC fruits",
	"contact": "09987654321"
	"owner_id": <owner_id>
});
db.suppliers.insertOne({
	"name": "DEF Fruits",
	"contact": "093216543987",
	"address": [
			{
				"street": "123 San Jose St.",
				"city": "Manila"
			},
			{
				"street": "367 Gil Puyat.",
				"city": "Makati"
			}
		]
})