// 2. Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
	{ $match: {
		"onSale": true
	}},
	{ $count: "fruitsOnSale"}
])
// 3. Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
	{ $match: {
		stock: {$gte: 20 }
	}},
	{ $count: "enoughStock"}

]);

// 4. Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
  {$match: {onSale: true}},
  {
    $group: {_id: "$supplier_id",
      avg_price: { $avg: "$price" }},
  },
  
]);

// 5. Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
  {$match: {onSale: true},
	},
  {$group: {
      _id: "$supplier_id",
      max_price: { $max: "$price" }}
  },
]);

// 6. Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
  {$match: {onSale: true}
},
  {$group: {_id: "$supplier_id",
      min_price: { $min: "$price" }}
  },
]);



MongoDB $project
		https://docs.mongodb.com/manual/reference/operator/aggregation/project/
	MongoDB $sort
		https://docs.mongodb.com/manual/reference/operator/aggregation/sort/
	MongoDB $unwind
		https://docs.mongodb.com/manual/reference/operator/aggregation/unwind/
	MongoDB $count
		https://docs.mongodb.com/manual/reference/operator/aggregation/count/
	MongoDB $avg
		https://docs.mongodb.com/manual/reference/operator/aggregation/avg/
	MongoDB $min
		https://docs.mongodb.com/manual/reference/op