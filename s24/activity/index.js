// console.log("QUITER'S NEVER WIN!")

// Exponent Operator
const getCube = Math.pow(2,3)
console.log(`The cube of 2  ${getCube}`)

// Template Literals

// Array Destructuring

const address = ["258", "Washington Ave NW", "California", "90011"];
console.log(`I live at ${address}`)

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
const {
	name,
	species,
	weight,
	measurement
} = animal
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
numbers.forEach((e)=> console.log(e));

let reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue);
console.log(reduceNumber);


// Javascript Classes

class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
};
let dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}