// console.log("QUITER'S NEVER WIN!")

/* ES6 UPDATES*/
// Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum)

const secondNum = Math.pow(8,2)
console.log(secondNum)

// Template Literals
/*
	- Allows us to write things without using the concatenation operator (+)
	- Greatly
*/
let name = "John"

//  Pre-template literal
let message = "Hello " + name + "! Welcome to programming!"
console.log("Message without template literal: " + message)

// String using tempate literals
// Uses backticks (``)
message = `Hello ${name}! Welcome to programming!`
console.log("Message with template literal: " + message)

const anotherMessage = `
${name} attend a math competition. 
He won it by solving the proble 8 ** 2 with the solution of ${firstNum}.`

console.log(anotherMessage)

/*
	- Template literals allow us to write strings with embedded JavaScript expressions
	- expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals
*/

const interestRate = .1
const principal = 1000

console.log(`The interest on your savings account is: ${principal * interestRate}`)

/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability

		Syntax:
        let/const [variableName1, variableName2, variableName3] = arrayName;

*/
const fullName = ["Juan", "Dela", "Cruz"]

// Pre-array Destructuring ==================================
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`)

// Array Destructuring ======================================
const [firstName, middleName, lastName] = fullName

console.log(firstName)
console.log(middleName)
console.log(lastName)

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you! `)
console.log(`Hello ${fullName}! It's nice to meet you!`)

//  Objects Destructuring ===================================
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects

	Syntax:
		let/const ${propertyName} ${propertyName} ${propertyName} = objectNam
*/
const person = {
	givenName: "Juana",
	maidenName: "Dela",
	familyName: "Cruz",
}

// Pre-Object
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's nice to meet you`)

// Object Destructuring
// variable deconstructing
const {givenName, maidenName, familyName} = person
console.log(givenName)
console.log(maidenName)
console.log(familyName)

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's nice to meet you`)


/*S
	Passing the values using destructured properties of the object
	Syntax:
		function function  (propertyName, propertyName, propertyName, ){
			return statement (parameters)
		}
		funcName(object)
*/

// Function to destructuring our object ====================
function getFullName({ givenName, maidenName, familyName }){
	console.log(` ${givenName} ${maidenName} ${familyName}`)
}
getFullName(person)


// pre-Arrow Functions======================================
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

// function printFullname(firstName, lastName){
// 	console.log(`${firstName} ${lastName} `)
// }
// printFullname("John", "Smith")

//  Arrow Function (=>)
/*
	Syntax:
		let/const = (parameterA, parameterB) => {
			statement
		}
		or (if single-line no need for curly brackets)

		let/const variableName = () => statement


*/
const printFullname =  (firstName, lastName) => {
	console.log(`${firstName} ${lastName} `)
}
printFullname("Jane", "Smith")

const hello = () => {
	console.log("Hello")
}
hello()

const sum = (x,y) => x + y;

let total = sum(1,1);
console.log(total);

person.talk = () => "Helloooooo!"
console.log(person.talk());


//  Function with default argument value
const greet = (name = "User") => {
	return `Good morning, ${name}!`
}

console.log(greet());
console.log(greet("Ariana"));

// Class-Based Object Blueprints

/*
    allows the creation/instantation of object using classes as blue prints

    Syntax:
        class className {
            constructor(objectPropertyA, objectPropertyB){
                this.objectPropertyA = objectpropertyA;
                this.objectPropertyB = objectpropertyB;
            }
        };

*/

class Car {
    constructor(brand, model, year){
        this.brand = brand;
        this.model = model;
        this.year = year;
    }
}

const myCar = new Car();
console.log(myCar);

// Values of properties may be assigned after creation/instantiation of an object
myCar.brand = "Ford";
myCar.model = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);


const myNewCar = new Car("Toyata", "Vios", 2021);
console.log(myNewCar);