// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// Insert documents (Create)
/*
    Insert One Document
        db.collectionName.insertOne({
            "fieldA": "valueA",
            "fieldB": "valueB"
        });

    Insert Many Documents
        db.collectionName.insertMany([
            {
                "fieldA": "valueA",
                "fieldB": "valueB"
            },
            {
                "fieldA": "valueA",
                "fieldB": "valueB"
            }
        ]);
*/
db.users.insertOne({
    "firstName": "Janie",
    "lastName": "Jardeleza",
    "mobileNumber": "+639123456789",
    "email": "janiejardeleza@mail.com",
    "company": "Zuitt"
});

db.users.insertMany([
        {
            firstName: "Stephen",
            lastName: "Hawking",
            age: 76,
            contact: {
                phone: "87654321",
                email: "stephenhawking@mail.com"
            },
            courses: ["Python", "React", "PHP", "CSS"],
            department: "none"
        },
        { 
            firstName: "Neil",
            lastName: "Armstrong",
            age: 82,
            contact: {
               phone: "98765432",
               email: "neilarmstrong@mail.com"
            },
            courses: ["React", "Laravel", "Sass"],
            department: "none"            
        }
    ]);

db.courses.insertMany([
        {
            "name": "Javascript 101",
            "price": 5000,
            "description": "Introduction to Javascript",
            "isActive": true
        },
        {
            "name": "HTML 101",
            "price": 2000,
            "description": "Introduction to HTML",
            "isActive": true
        }
    ]);


// Finding documents (Read/Retrieve)
/*
    Syntax:
        - db.collectionName.find(); - find all
        - db.collectionName.find({field: value}); - all document that will match the criteria
        - db.collectionName.findOne({field: value}) - first document that wll match the criteria
        - db.collectionName.findOne({}) - find first document
*/

db.users.find();
db.users.find({ "firstName": "Stephen" });
db.users.findOne({});


// Updating/Replacing/Modifying documents (Update)
/*
    Syntax:
    Updating One Document
        db.collectioName.updateOne(
            {
                criteria: value
            },
            {
                $set: {
                    fieldToBeUpdated: value
                }
            }
        );
            - update the first matching document in our collection

    Multiple/Many Documents
    db.collectionName.updateMany(
        {
            criteria: value
        },
        {
            $set: {
                fieldToBeUpdated: value
            }
        }
    );

        - update multiple documents that matched the criteria
*/
db.users.insertOne({
    "firstName": "Test",
    "lastName": "Test",
    "mobileNumber": "+639123456789",
    "email": "test@mail.com",
    "company": "none"
});

db.users.updateOne(
    {
        "firstName": "Test"
    },
    {    
        $set:  {
                "firstName": "Bill",
                "lastName": "Gates",
                "mobileNumber": "123456789",
                "email": "billgates@mail.com",
                "company": "Microsoft",
                "status": "active"
        }
    }
);

db.courses.updateOne(
    {
        "name": "HTML 101"
    },
    {
        $set:{
            "isActive": false
    }
    }
);

db.courses.updateMany(
    {},
    {   $set:{
            "enrollees": 10
        }
    }
);

// Rename fieldname
db.users.updateMany(
    {},
    {   
        $rename:{
            "department": "dept"
        }
    }
);

// Remove field
db.users.updateOne(
    {
        "firstName": "Bill" //criteria
    },
    {    
        $unset:  {
            "status": "active"
        }
    }
);

// Deleting Documents
/*
    Syntax:
        Deleting One Document:
            db.collectionName.deleteOne({"critera": "value"})

        Deleting Multiple Documents:
            db.collectionName.deleteMany({"criteria": "value"})
*/

db.users.deleteOne({
    "company": "Microsoft"
});

db.users.deleteMany({
    "dept": "none"
});

db.users.deleteMany({});